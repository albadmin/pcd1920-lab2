package inner;

/**
 * A wrapper data structure containing an array of int. 
 */
public class ArrayOfIntsDataStructure {
    
	/** 
	 * A fixed size array of int storing the elements. 
	 * */
    private int[] arrayOfInts = null;
    /**
     * The next available slot for element insertion.
     * */
    private int nextFreeSlot = 0;
    
    /**
     * Sole constructor of the ADT
     * */
    public ArrayOfIntsDataStructure(int size) {}
    
    /**
     * Adds an element to the array.
     * 
     * @param value: an int to be added into the list.
     * @return: -1 in case there is no available slot in the array, otherwise the element (value) 
     * */
    public int add(int value) {
    	
    	throw new UnsupportedOperationException("");    	
    }
    
    /**
     * Returns an Iterator instance, iterating over odd array indexes. 
     * */
    public Iterator oddIterator() {
        
    	throw new UnsupportedOperationException("");
    }
    
    /**
     * Returns an Iterator instance, iterating over even array indexes. 
     * */    
    public Iterator evenIterator() {

    	throw new UnsupportedOperationException("");
    }
    
    interface Iterator extends java.util.Iterator<Integer> { } 

    // Inner class implements the DataStructureIterator interface,
    // which extends the Iterator<Integer> interface    
    private class EvenIterator implements Iterator {
        
        // Start stepping through the array from the beginning
        private int nextIndex = 0;
        
        public boolean hasNext() {

        	throw new UnsupportedOperationException("");
        }        
        
        public Integer next() {
            
        	throw new UnsupportedOperationException("");
        }
    }

    // Inner class implements the DataStructureIterator interface,
    // which extends the Iterator<Integer> interface    
    private class OddIterator implements Iterator {
        
        // Start stepping through the array from the beginning
        private int nextIndex = 0;
        
        public boolean hasNext() {

        	throw new UnsupportedOperationException("");
        }        
        
        public Integer next() {
            
        	throw new UnsupportedOperationException("");
        }
    }
    
    
    public static void main(String s[]) {
        
    	ArrayOfIntsDataStructure ds = new ArrayOfIntsDataStructure(10);
    	Iterator even = ds.evenIterator();
    	while(even.hasNext()) System.out.println(even.next());
    }
}